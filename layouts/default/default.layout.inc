name = omestrap default layout
template = default-layout

; Regions
regions[navigation]     = Navigation
regions[content_top]    = Content top
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[footer]         = Footer
