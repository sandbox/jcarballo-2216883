<div class="l-page container">
  <header class="l-header" role="banner">
    <div class="l-branding">
      <?php
        $link_front_page = $front_page;
        if ($is_front) {
          $link_front_page = '#';
        }
      ?>
      <a href="<?php print $link_front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
    </div>
  </header>

  <section class="l-main">
    <nav class="l-navigation">
      <?php print $breadcrumb; ?>
      <?php print render($page['navigation']); ?>
    </nav>
    <main class="l-content" role="main">
      <a id="main-content"></a>
      <?php print $messages; ?>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <div class="page-header">
          <h1><?php print $title; ?></h1>
        </div>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print render($tabs); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <div class="l-entry-content">
        <?php print render($page['content_top']); ?>
        <?php print render($page['content']); ?>
      </div>
    </main>

    <?php print render($page['sidebar_first']); ?>
  </section>

  <footer class="l-footer" role="contentinfo">
    <?php print render($page['footer']); ?>
  </footer>
</div>
