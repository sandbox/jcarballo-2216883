<article<?php print $attributes; ?>>
  <header>
    <?php print render($content['field_partner_logo']); ?>
  </header>
  <div<?php print $content_attributes; ?>>
    <?php print render($title_prefix); ?>
      <h2<?php print $title_attributes; ?>>
        <?php print $title; ?>
      </h2>
    <?php print render($title_suffix); ?>
    <?php
      hide($content['field_partner_logo']);
      print render($content);
    ?>
  </div>
</article>
