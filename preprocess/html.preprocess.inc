<?php

/**
 * @file
 * Contains a pre-process hook for 'html'.
 */

/**
 * Implements hook_preprocess_html().
 */
function omestrap_preprocess_html(&$variables) {
  drupal_add_css('http://fonts.googleapis.com/css?family=Merriweather', array('type' => 'external'));

  drupal_add_css('http://fonts.googleapis.com/css?family=Lato', array('type' => 'external'));
}
