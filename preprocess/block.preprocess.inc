<?php

/**
 * @file
 * Contains a pre-process hook for 'block'.
 */

/**
 * Implements hook_preprocess_blok().
 */
function omestrap_preprocess_block(&$variables) {
  /* Set shortcut variables. Hooray for less typing! */
  $block_id = $variables['block']->module . '-' . $variables['block']->delta;
  $classes = &$variables['classes_array'];
  $title = &$variables['block']->subject;
  $title_classes = &$variables['title_attributes_array']['class'];
  $content_classes = &$variables['content_attributes_array']['class'];

  /* Uncomment the line below to see variables you can use to target a field */
  #print $block_id . '<br/>';

  switch ($block_id) {
    // Main Menu
     case 'system-main-menu':
      $title = '<none>';
      break;
   }
}

